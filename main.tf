terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "us-east-2"
}

# Modules 
module "aws_vpc" {
  source    = "./modules/vpc"
  vpc_cidr  = var.cidr
  ipv6_cidr = var.ipv6_cidr_block
  vpc_name  = var.vpc_name
  vpc_tag   = var.vpc_tag
  tenancy   = var.tenancy_status
}

module "aws_subnet" {
  source              = "./modules/subnet"
  id_vpc              = module.aws_vpc.vpcid.id
  subnet_cidr_block   = var.subnet_cidr
  public_ip_on_launch = var.public_ip_on_launch
  availability_zone   = var.availability_zone
  subnet_name         = var.subnet_name
  subnet_tag          = var.subnet_tag

}

module "aws_internet_gateway" {
  source  = "./modules/igw"
  vpc_id  = module.aws_vpc.vpcid.id
  ig_name = var.ig_name
  ig_tag  = var.ig_name

}



module "aws_route_table" {
  source           = "./modules/routetable"
  vpc_id_for_route = module.aws_vpc.vpcid.id
  route_name       = var.route_name
  route_tag        = var.route_tag

  route = [{
    cidr_block = var.cidr_for_route
    gateway_id = module.aws_internet_gateway.ig_output.id
  }]
  subnet_id_for_association = module.aws_subnet.subnet_output.id
}
# 
module "aws_security_group" {
  source            = "./modules/securitygroup"
  vpc_id_for_sg     = module.aws_vpc.vpcid.id
  custom_cidr_block =  var.custom_cidr_block_for_ingress
  sg-name           = var.sg-name
}

module "aws_instance" {
  source                = "./modules/ec2"
  ami_id                = var.ami_id
  ec2_type              = var.ec2_type
  status_public_ip      = var.status_public_ip
  ec2_availability_zone = module.aws_subnet.subnet_output.availability_zone
  iam_profile           = var.iam_profile
  key_name_for_ec2      = var.key_name_for_ec2
  security_group_id     = [module.aws_security_group.security_group_output.id]
  subnet_id_for_ec2     = module.aws_subnet.subnet_output.id
  ec2_name              = var.ec2_name
}
