variable "cidr" {
  type        = string
  description = "This is for vpc"
}

variable "subnet_cidr" {
  type        = string
  description = "This variable for subnet cidr block"
}

variable "ipv6_cidr_block" {
  type        = string
  description = "Enter the value for assign_generated_ipv6_cidr_block  true/false"
  default     = "false"

}

variable "tenancy_status" {
  type        = string
  description = "Enter the instant_tenancy status default/dedicated/host"
  default     = "default"

}

variable "vpc_name" {
  type        = string
  description = "Enter the vpc name"
  default     = null

}


variable "vpc_tag" {
  type        = string
  description = "Enter the vpc tag"

}

variable "availability_zone" {
  type        = string
  description = "Enter the availability in which you want to launch subnet"

}

variable "public_ip_on_launch" {
  type        = string
  description = "Enter the status of public_ip_on_launch true/false"
  default     = "false"

}


variable "subnet_name" {
  type        = string
  description = "Enter the subnet name"
  default     = "My-subnet"
}

variable "subnet_tag" {
  type        = string
  description = "Enter the subnet tag"
  default     = "Created by modules"

}
