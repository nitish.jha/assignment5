variable "vpc_cidr" {
  type = string
  description = "This is cidr block for VPC"
  
}
variable "vpc_name" {
  type = string
  description = "This is for name of vpc"
}

variable "vpc_tag" {
    type = string
  description = "This is for tag in vpc"
  
}

variable "ipv6_cidr" {
  type = string
  description = "Enter the ipv6_cidr true/false"
}

variable "tenancy" {
  type = string
  description = "Enter the instant_tenancy status default/dedicated/host"
}

