# Create a VPC
resource "aws_vpc" "vpc_module" {
  cidr_block = var.vpc_cidr
  assign_generated_ipv6_cidr_block = var.ipv6_cidr
  instance_tenancy  = var.tenancy
  tags = {
    Name = var.vpc_name
    name = var.vpc_tag

  }
}
