resource "aws_subnet" "subnet_module" {
  vpc_id     = var.id_vpc
  cidr_block = var.subnet_cidr_block
  availability_zone  = var.availability_zone
  map_public_ip_on_launch = var.public_ip_on_launch


  tags = {
    Name = var.subnet_name
    name = var.subnet_tag
  }
}

