variable "id_vpc" {
  type = string
  description = "This is for vpc id for subnet creation"
}

variable "subnet_cidr_block" {
  type = string
  description = "This is for cidr block for subnet"
}

variable "availability_zone" {
  type = string
  description = "Enter the availability_zone in which subnet will launch"
}


variable "public_ip_on_launch" {
  type = string
  description = "Enter the public_ip_on_launch status true/false"  
}

variable "subnet_name" {
  type = string
  description = "Enter the subnet name"
  
}

variable "subnet_tag" {
  type = string
  description = "Enter the subnet tag"
  
}
